﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RL_MVVM.Model
{
    public class RequestManager
    {
        public class RequestRes
        {
            public List<string> result { get; set; }
        }

        public List<string> Database = new List<string>();

        public List<string> Trades = new List<string>();

        private string RequestTrade = "http://localhost:55500/Home/Index";

        private string UpdateDatabase = "http://localhost:55500/Home/Database";

        public async Task<List<string>> TradeRequest()
        {
            await Task.Run(() =>
            {
                try
                {
                    WebClient client = new WebClient();

                    string data = client.DownloadString(RequestTrade);

                    data = data.Remove(0, 1);
                    data = data.Remove(data.Length - 1);

                    data = data.Replace("\\", "");

                    var json = JsonConvert.DeserializeObject<RequestRes>(data);

                    Trades.Clear();

                    Trades = json.result;

                    return json.result;
                }
                catch
                {
                    return new List<string>();
                }
            });

            return new List<string>();
        }

        public async Task<List<string>> DatabaseRequest()
        {
            await Task.Run(() =>
            {
                try
                {
                    WebClient client = new WebClient();

                    string data = client.DownloadString(UpdateDatabase);

                    data = data.Remove(0, 1);
                    data = data.Remove(data.Length - 1);

                    data = data.Replace("\\", "");

                    var json = JsonConvert.DeserializeObject<RequestRes>(data);

                    Database.Clear();

                    Database = json.result;

                    return json.result;
                }
                catch
                {
                    return new List<string>();
                }
            });

            return new List<string>();
        }

    }
}
