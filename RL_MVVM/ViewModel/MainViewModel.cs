﻿using RL_MVVM.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace RL_MVVM.ViewModel
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private string lastUpdate;
        private bool updating;
        public RequestManager manager;
        public ObservableCollection<string> DBItems { get; set; }

        public string LastUpdate
        {
            get { return lastUpdate; }
            set
            {
                lastUpdate = value;
                OnPropertyChanged(nameof(LastUpdate));
            }
        }

        public bool Updating
        {
            get { return updating; }
            set
            {
                updating = value;
                OnPropertyChanged(nameof(Updating));
            }
        }



        public MainViewModel()
        {
            Updating = true;
            DBItems = new ObservableCollection<string>();
            manager = new RequestManager();
            DBItems.Add("Update List Please");
        }


        private RelayCommand refreshCommand;

        public RelayCommand RefreshCommand
        {
            get
            {
                return refreshCommand ??
                  (refreshCommand = new RelayCommand(async obj =>
                  {
                      LastUpdate = $"Sending request & start parsing";

                      Updating = false;

                      await manager.DatabaseRequest();

                      DBItems.Clear();

                      foreach (var item in manager.Database)
                      {
                          DBItems.Add(item);
                      }

                      string UpdateTime = DateTime.Now.ToString("hh:mm:ss");

                      int ItemsCount = DBItems.Count;

                      LastUpdate = $"Last Update time: {UpdateTime} -> Items count in database: {ItemsCount}";

                      Updating = true; // Button enable

                  }));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
