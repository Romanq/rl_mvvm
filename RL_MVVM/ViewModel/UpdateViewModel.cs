﻿using RL_MVVM.Model;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace RL_MVVM.ViewModel
{
    public class UpdateViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<string> Parsed { get; set; }

        private RequestManager manager;

        private bool autoupdate;

        public bool AutoUpdate
        {
            get { return autoupdate; }
            set
            {
                autoupdate = value;
                OnPropertyChanged(nameof(AutoUpdate));
            }
        }

        private int freq;

        public int Freq
        {
            get { return freq; }
            set
            {
                freq = value;
                OnPropertyChanged(nameof(Freq));
            }
        }

        private RelayCommand parseTradesFrequently;

        public RelayCommand ParseTradesFrequently
        {
            get
            {
                return parseTradesFrequently ??
                  (parseTradesFrequently = new RelayCommand(async obj =>
                  {
                      AutoUpdate = false;

                      Parsed.Clear();

                      while (Freq != 0)
                      {
                          await manager.TradeRequest();

                          foreach (var rets in manager.Trades)
                          {
                              Parsed.Add(rets);
                          }

                          Freq--;

                          await Task.Run(() => Thread.Sleep(5000));

                          Parsed.Clear();
                      }

                      Parsed.Clear();

                      AutoUpdate = true;
                  }));
            }
        }


        public UpdateViewModel()
        {
            manager = new RequestManager();
            Parsed = new ObservableCollection<string>();
            Freq = 1;
            AutoUpdate = true;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
