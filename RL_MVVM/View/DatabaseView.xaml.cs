﻿using System.Windows;
using RL_MVVM.View;

namespace RL_MVVM
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Garage_Finder finderWnd = new Garage_Finder();
            finderWnd.Show();
        }
    }
}
